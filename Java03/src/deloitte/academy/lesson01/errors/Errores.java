package deloitte.academy.lesson01.errors;

public enum Errores {
	FATAL("HA OCURRIDO UN ERROR INESPERADO Y EL SISTEMA NO SE HA PODIDO RECUPERAR"),
	WRONG("HA OCURRIDO UN ERROR PERO EL SISTEMA SE HA RECUPERADO");

	public String description;

	/**
	 * 
	 * @param description la cual ser� dada al error
	 */
	Errores(String description) {
		this.description = description;
	}

	/*
	 * Getter del atributo de la descripci�n
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter del atributo de la descripci�n
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
