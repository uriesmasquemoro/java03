package deloitte.academy.lesson01.operations;

import java.util.ArrayList;
import java.util.logging.Logger;

import deloitte.academy.lesson01.producto.Producto;

public class Operations {
	// Logger required to display alert or error messages to the user
	private static final Logger LOGGER = Logger.getLogger(Operations.class.getName());
	public static ArrayList<Producto> listaProductos = new ArrayList<Producto>();
	public static ArrayList<String> listaCategorias = new ArrayList<String>();
	public static ArrayList<Integer> listaVendidos = new ArrayList<Integer>();

	/**
	 * Este m�todo permite agregar un producto al stock, clasific�ndolo por
	 * categor�a y a�adiendo su costo y cantidad.
	 * 
	 * @param nombre    es el nombre que ser� asignado al producto
	 * @param categoria la categor�a que estar� asociada al producto
	 * @param costo     que se dar� al producto para su venta
	 * @param cantidad  cantidad inicial en stock del producto
	 */
	public static void agregarProducto(String nombre, String categoria, int costo, int cantidad) {
		String nombreUpper = nombre.toUpperCase();
		String categoriaUpper = categoria.toUpperCase();
		Producto producto = new Producto(nombreUpper, categoriaUpper, costo, cantidad);

		if (!Operations.listaCategorias.contains(categoriaUpper)) {
			Operations.listaCategorias.add(categoriaUpper);
		}

		Operations.listaProductos.add(producto);
		LOGGER.info("Producto agregado con �xito...\n");
	}

	/**
	 * Este m�todo permite mostrar en consola los productos registrados por
	 * categor�a, no se requiere de ning�n par�metro
	 */
	public static void mostrarProductos() {
		System.out.println("= MOSTRANDO PRODUCTOS: ");
		for (String categoria : Operations.listaCategorias) {
			System.out.println("\n" + categoria);
			for (Producto producto : Operations.listaProductos) {
				if (producto.categoria.equals(categoria)) {
					System.out.println(producto.cantidad + " " + producto.nombre + " $" + producto.precio);
				}
			}
		}
	}

	/**
	 * Esta funci�n permite la compra de un producto y restando su cantidad en stock
	 * 
	 * @param nombre del producto que se quiere comprar, no es necesario ning�n otro
	 *               par�metro
	 */
	public static void comprarProducto(String nombre) {
		var indice = 0;

		for (Producto producto : Operations.listaProductos) {
			if (producto.nombre.equals(nombre.toUpperCase())) {
				if (Operations.listaProductos.get(indice).cantidad == 0) {
					LOGGER.info("No existe el producto en el stock...\n");
					return;
				}

				Operations.listaProductos.get(indice).cantidad -= 1;
				Operations.listaVendidos.add(producto.precio);
				LOGGER.info("Compra realizada con �xito...\n");
				return;
			}

			indice++;
		}
		
		LOGGER.info("No existe ese producto...\n");
	}
	
	public static void obtenerValorDeVentas() {
		Integer total = 0;
		
		for(Integer integer : Operations.listaVendidos) {
			total += integer;
		}
		
		LOGGER.info("El valor total de ventas es de: $" + total);
	}
}
