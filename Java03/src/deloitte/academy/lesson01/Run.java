package deloitte.academy.lesson01;

import java.util.Scanner;
import java.util.logging.Logger;

import deloitte.academy.lesson01.errors.Errores;
import deloitte.academy.lesson01.operations.Operations;

/**
 * Clase principal en la cual son ejecutadas totas las operaciones necesarias
 * para la gesti�n de los productos que se compran y registran
 * 
 * @author rubemendoza
 *
 */
public class Run {
	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());

	public static void main(String[] args) {
		/*
		 * En el m�todo main s�lo se hace el llamado del men�
		 */
		menu();
	}

	/**
	 * En el m�todo de men� se efect�a toda la l�gica para llevar a cabo el proceso
	 * de gesti�n
	 */
	public static void menu() {
		var ejecutar = true;
		Scanner sc = new Scanner(System.in);

		try {
			do {
				System.out.println("===========      MEN�     ===========");
				System.out.println("1. Agregar producto");
				System.out.println("2. Mostrar productos");
				System.out.println("3. Comprar producto");
				System.out.println("4. Total de ventas");
				System.out.println("5. Salir");
				System.out.print("\nSelecciona una opci�n: ");

				String option = sc.nextLine();

				switch (option) {
				case "1":
					System.out.println("Registrando producto...\n");

					System.out.print("\nIngresa el nombre del producto: ");
					String nombre = sc.nextLine();
					System.out.print("Ingresa la categor�a a la que pertenece: ");
					String categoria = sc.nextLine();
					System.out.print("Ingresa su costo unitario: ");
					Integer costo = Integer.parseInt(sc.nextLine());
					System.out.print("Ingresa su cantidad en stock: ");
					Integer cantidad = Integer.parseInt(sc.nextLine());

					Operations.agregarProducto(nombre, categoria, costo, cantidad);
					break;
				case "2":
					Operations.mostrarProductos();
					break;
				case "3":
					System.out.print("Ingresa el nombre del producto que deseas comprar: ");
					String name = sc.nextLine();
					Operations.comprarProducto(name);
					break;
				case "5":
					sc.close();
					LOGGER.info("Saliendo...\n");
					ejecutar = !ejecutar;
					break;
				case "4":
					Operations.obtenerValorDeVentas();
					break;
				default:
					LOGGER.info(Errores.WRONG.getDescription());
					LOGGER.info("\n\nIngresa una opci�n v�lida...");
					break;
				}
			} while (ejecutar);
		} catch (Exception e) {
			sc.close();
			LOGGER.info(Errores.FATAL.getDescription());
		}
	}
}
