package deloitte.academy.lesson01.producto;

public class Producto {
	public String nombre, categoria;
	public int precio;
	public int cantidad;

	public Producto(String nombre, String categoria, int precio, int cantidad) {
		this.nombre = nombre;
		this.categoria = categoria;
		this.precio = precio;
		this.cantidad = cantidad;
	}
}
